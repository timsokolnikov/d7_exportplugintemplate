﻿using PluginInterface;
using PluginInterface.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using UIs;

namespace DatacolPluginTemplate
{
    public class TestPluginStaticSetting : PluginStaticSetting
    {
        
        public TestPluginStaticSetting()
            : base()
        {
            mStringOption = "smth";

            mCheckComboParameter = "";
            mComboParameter = "";
            mCheckComboParameterRange = "";
        }

        private string mStringOption;
        [Category("Basic")]
        [DisplayNameAttribute("StringOption")]
        [DescriptionAttribute("StringOptionDesc")]
        public string StringOption
        {
            get { return mStringOption; }
            set { mStringOption = value; }
        }

        private string mIntOption;
        [Category("Basic")]
        [DisplayNameAttribute("IntOption")]
        [DescriptionAttribute("IntOptionDesc")]
        public string IntOption
        {
            get { return mIntOption; }
            set { mIntOption = value; }
        }

        private bool mBoolOption;
        [Category("Basic")]
        [DisplayNameAttribute("BoolOption")]
        [DescriptionAttribute("BoolOptionDesc")]
        [TypeConverter(typeof(YesNoConverter))]
        public bool BoolOption
        {
            get { return mBoolOption; }
            set { mBoolOption = value; }
        }

        private string mFilename;
        [Category("Basic")]
        [DisplayNameAttribute("Filepath")]
        [DescriptionAttribute("InputDataFilePath")]
        [Editor(typeof(FileSelectorTypeEditor), typeof(UITypeEditor))]
        public string Filename
        {
            get { return mFilename; }
            set { mFilename = value; }
        }

        private string mExtraReportFolderPath;
        [Category("Basic")]
        [DisplayNameAttribute("FolderPath")]
        [DescriptionAttribute("ExtraReportFolderPathDesc")]
        [Editor(typeof(FolderNameEditor), typeof(UITypeEditor))]
        public string ExtraReportFolderPath
        {
            get { return mExtraReportFolderPath; }
            set { mExtraReportFolderPath = value; }
        }

        private string mPluginFilename;
        [Category("Extensions")]
        [DisplayNameAttribute("PluginDataProcess")]
        [DescriptionAttribute("PluginDataProcessDesc")]
        [Editor(typeof(FileSelectorTypeEditor), typeof(UITypeEditor))]
        public string PluginFilename
        {
            get { return mPluginFilename; }
            set { mPluginFilename = value; }
        }


        private bool mCacheOn;
        [Category("Loading")]
        [DisplayNameAttribute("CacheOn")]
        [DescriptionAttribute("CacheOnDesc")]
        [TypeConverter(typeof(YesNoConverter))]
        public bool CacheOn
        {
            get { return mCacheOn; }
            set { mCacheOn = value; }
        }

        private List<string> mProxyList;
        [Category("Basic")]
        [DisplayNameAttribute("ListToEditAsMultilineStringOption")]
        [DescriptionAttribute("ListToEditAsMultilineStringOptionDesc")]
        [Editor(typeof(MultiLineEditorListBased), typeof(UITypeEditor))]
        public List<string> ProxyList
        {
            get { return mProxyList; }
            set { mProxyList = value; }
        }

        private string mCheckComboParameter;
        [Category("Basic")]
        [DisplayNameAttribute("CheckboxListOption")]
        [DescriptionAttribute("CheckboxListOptionDesc")]
        [Editor(typeof(CheckedListBoxUiTypeEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(DisableDirectEditConverter))]
        [PropertyList("Значение 1", "Значение 2")]
        [SelectAllEnabled(true)]
        public string CheckComboParameter
        {
            get { return mCheckComboParameter; }
            set { mCheckComboParameter = value; }
        }


        private string mCheckComboParameterRange;
        [Category("Basic")]
        [DisplayNameAttribute("CheckboxListWithRangesOption")]
        [DescriptionAttribute("CheckboxListWithRangesOptionDesc")]
        [Editor(typeof(CheckedListBoxRangeUiTypeEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(DisableDirectEditConverter))]
        [PropertyList("1", "2", "3", "6", "7", "8")]
        [RangeSelect("Test1", "1;2;3")]
        [RangeSelect("Test2", "6;7;8")]
        [SelectAllEnabled(true)]
        public string CheckComboParameterRange
        {
            get { return mCheckComboParameterRange; }
            set { mCheckComboParameterRange = value; }
        }

        private string mComboParameter;
        [Category("Basic")]
        [DisplayNameAttribute("ComboboxListOption")]
        [DescriptionAttribute("ComboboxListOptionDesc")]
        [TypeConverter(typeof(ParameterComboConverter))]
        [PropertyList("Значение 1", "Значение 2")]
        public string ComboParameter
        {
            get { return mComboParameter; }
            set { mComboParameter = value; }
        }

    }
}
