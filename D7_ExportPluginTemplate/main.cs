﻿using System;
using System.Data.Linq;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Collections.Specialized;
using System.IO;
using System.Data;
using System.Net;
using System.Threading;
using System.Globalization;
using System.Linq;
using PluginInterface;
using DatacolPluginTemplate;

namespace Plugin
{
    /// <summary>
    /// Простейшая реализация интерфейса плагина, сборку с этим классом мы будем подгружать динамически,
    /// главный проект не имеет на нее ссылок! Класс реализует интерфейс плагина, 
    /// для унифицированной работы со всеми плагинами а так же для того, чтобы можно было динамически найти класс в сборке.
   /// </summary>
    public class HandlerClass : PluginInterface.IPlugin
    {
        /// <summary>
        /// Обработчик плагина
        /// </summary>
        /// <param name="parameters">Словарь параметров: ключ - имя параметра (string), 
        /// значение - содержимое параметра (object, который в зависимости от типа плагина (задается в parameters["type"])
        /// и ключа приводится к тому или иному типу) </param>
        /// <param name="error">Переменная (string), в которую возвращается ошибка работы плагина, 
        /// если таковая произошла. Если ошибки не произошло, данная переменная должна оставаться пустой строкой</param>
        /// <returns>Возвращаемое значение - это объект, который может иметь тот или иной тип,
        /// в зависимости от типа плагина (задается в  parameters["type"])</returns>
        public object pluginHandler(Dictionary<string, object> parameters, out string error)
        {
            try
            {
                error = "";

                ///Получаем имя кампании из словаря parameters
                string campaignname = parameters["campaignname"].ToString();
                string type = parameters["type"].ToString();

                //проверка типа плагина
                if (type != "export_plugin") throw new Exception("Неправильный тип плагина");

               
                //параметр таблица собранных данных (содержит все собранные с текущей страницы группы данных)
                DataTable dt = (DataTable)parameters["datatable"];
                //параметр список названий колонок таблицы собранных данных
                List<string> columnNames = (List<string>)parameters["columnnames"];
                
                //пример обращения к параметру визуальной конфигурации
                //(parameters[PluginMiscStr.VisualConfigParameterName] as TestPluginStaticSetting).SomeParameter;

                #region Простой пример обработки данных в таблице
                
                foreach (DataRow row in dt.Rows)
                {
                    //Названия колонок в таблице соответствуют наименованиям полей данных в кампании (настройке) Datacol
                    //Чтобы поддерживались разные наименования, их стоит задать как параметры плагина. Как вариант - в визуальной конфигурации
                    string name_dat = row["название"].ToString();
                    string price_name_dat = row["цена"].ToString();
                    string description_dat = row["описание"].ToString();
                    string photos_dat = row["изображение"].ToString();
                    string url_dat = row["URL"].ToString();

                    //Логика экспорта
                    //...
                    //...
                    //...
                }

                #endregion

            }
            catch (Exception exp)
            {
                error = exp.Message;
            }

            return "";
        }

        #region Методы и свойства необходимые, для соответствия PluginInterface (обычно не используются при создании плагина)

        public void Init()
        {
            
        }

        public void Destroy()
        {
            //это тоже пока не надо
        }

        public string Name
        {
            get { return "Имя плагина"; }
        }

        public string Description
        {
            get { return "Описание плагина"; }
        }

        #endregion
    }
}
